const mongoose = require('mongoose');
const passport = require('passport');
const Student = require('./models/student.js');

const csvInput = `
class;lastName;firstName;username
8a;Bornschein;Norman;nbornschein
8a;Buchwald;Leon;lbuchwald
8a;Kaerger;Kevin;kkaerger
8a;Klemmt;Raik Felix;rfklemmt
8a;Lehmann;Noel Etienne;nelehmann
8a;Martin;Marius;mmartin
8a;Rauscher;Hendrik;hrauscher
8a;Scheil;Richard;rscheil
8a;Schrimpf;Niklas;nschrimpf
8a;Witaseck;Rocco;rwitaseck
8a;Brauer;Lea Yvette;lybrauer
8a;Finke;Michelle;mfinke
8a;Gießler;Benita;bgiessler
8a;Kreissl;Sophie;skreissl
8a;Rehbohm;Franziska;frehbohm
8a;Schönau;Celina;cschoenau
8a;Taudte;Sina;staudte
8a;Tessin;Lea Helge;lhtessin
8a;Wagenhaus;Franziska;fwagenhaus
8a;Welzel;Jasmin;jwelzel
8a;Wötzel;Michelle;mwoetzel
8a;Zeise;Michelle;mzeise
8b;Bernstein;Oliver;obernstein
8b;Fritzsche;Max;mfritzsche
8b;Heller;Domenik;dheller
8b;Kirmse;Paul Niklas;pnkirmse
8b;Kleeberg;Tom;tkleeberg
8b;Reichel;Nelson;nreichel
8b;Schreiber;Ron;rschreiber
8b;Seiler;Tom;tseiler
8b;Buttler;Jasmin Leonie;jlbuttler
8b;Diez;Lisa;ldiez
8b;Gründonner;Sophie;sgruendonner
8b;Jokeit;Celina;cjokeit
8b;Krombholz;Anna;akrombholz
8b;Reichmann;Paulina;preichmann
8b;Richter;Annica;arichter
8b;Schildbach;Hannah Sophie;hsschildbach
8b;Schindler;Celia;cschindler
8b;Schmidt;Julia Marie;jmschmidt
8b;Schnorr;Clara;cschnorr
8b;Wakker;Christina;cwakker
9a;Fischer;Tamino;tfischer
9a;Gräfe;Nico;ngraefe
9a;Hahnemann;Nico;nhahnemann
9a;Kalisch;Pascal;pkalisch
9a;Klopfleisch;Marvin;mklopfleisch
9a;Kreßler;Jonas;jkressler
9a;Möbert;Ben Felix;bfmoebert
9a;Richter;Niklas;nrichter
9a;Rosenlöcher;Niklas;nrosenloecher
9a;Roth;Mike;mroth
9a;Schindler;Cedric;cschindler
9a;Szabo;Leon;lszabo
9a;Walther;Jonas;jwalther
9a;Armellini;Isabeau;iarmellini
9a;Babic;Dijana Alessandra;dababic
9a;Grüttner;Lara;lgruettner
9a;Haase;Patricia;phaase
9a;Handtke;Antonia;ahandtke
9a;Kieffer;Joan;jkieffer
9a;Körlin;Sunny Michelle;smkoerlin
9a;Schmidt;Yasmin;yschmidt
9b;Baun;Paul;pbaun
9b;Berndt;Paul;pberndt
9b;Dreger;Tamino Leander;tldreger
9b;Hesse;Lucas;lhesse
9b;Kaps;Jonathan Roman;jrkaps
9b;Krieger;Michel;mkrieger
9b;Malitz;René;rmalitz
9b;Müller;Toni;tmueller
9b;Riemann;Constantin;criemann
9b;Walther;Nico;nwalther
9b;Ziegler;Paul Eric;peziegler
9b;Abicht;Lara Josephin;ljabicht
9b;Barthel;Fabiane;fbarthel
9b;Hahnemann;Franka;fhahnemann
9b;Heyme;Josy;jheyme
9b;Lange;Patty;plange
9b;Ronneburg;Sarah;sronneburg
9b;Schäfer;Michelle;mschaefer
9b;Schröder;Jette-Florentine;jfschroeder
9b;Tönert;Jamila;jtoenert
9b;Yasar;Suzan;syasar
`

const functions = {
    makePassword: function() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (var i = 0; i < 6; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    },
    generatePasswords: function() {
        Student.find({}, (err, users) => {
            if (err) {
                console.log(err);
            } else {
                let returnedList = [];
                users.forEach((user) => {
                    let password = functions.makePassword();
                    let newUser = new Student({ username: user.username, firstName: user.firstName, lastName: user.lastName, class: user.class });
                    Student.register(newUser, password, (err, generatedUser) => {
                        if (err) {
                            return err;
                        } else {
                            returnedList.push({ username: user.username, firstName: user.firstName, lastName: user.lastName, password: password });
                            console.log(returnedList);
                            return returnedList;
                        }
                    });
                });
            }
        });

    },
    readCSV: function(file) {
        const parse = require('csv-parse/lib/sync');
        let input = file;

        if(input === '') {
            const records = parse(csvInput, {
                delimiter: ';',
                columns: true,
                skip_empty_lines: true
            });
            return records;
        } else {
            const records = parse(input, {
                delimiter: ';',
                columns: true,
                skip_empty_lines: true
            });
            return records;
        }
    }
};

module.exports = functions;
