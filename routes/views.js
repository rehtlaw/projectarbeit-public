const express = require('express'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    middleware = require('../middleware');

// model imports
const Student = require('../models/student');
const Station = require('../models/station');

// init
const router = express.Router();

// isLoggedIn middleware
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

// main routes
router.get('/', (req, res) => {
    // show all routes
    Station.find({}).exec()
        .catch(err => res.send(err))
        .then(stations => res.render('index', { stations: stations, currentUser: req.user, title: 'Alle Stationen'}));
});

router.get('/station/:id', (req, res) => {
    let liveMode = false;
    Station.find({live: true}, (err, docs) => {
        for(doc of docs) {
            if(doc.live) {
                liveMode = true;
            }
        }
        Station.findById(req.params.id).populate('students')
            .exec((err, station) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log(liveMode);
                    Student.find({}).exec()
                        .catch(err => console.log(err))
                        .then(allUsers => res.render('stationRegister', { liveMode: liveMode, station: station, allUsers: allUsers, currentUser: req.user, title: `Station ${station.name} - Registrieren` }));
                }
            });
    });
});

router.get('/station/:id/edit', (req, res) => {
    Station.findById(req.params.id, (err, doc) => {
        res.render('edit', {station: doc, title: `Station ${doc.name} - Editiern`});
    });
});


router.get('/login', (req, res) => {
    res.render('login', { title: 'Login' });
});


router.get('/admin', isLoggedIn, (req, res) => {
    let liveMode = false;
    Station.find({live: true}, (err, docs) => {
        for(doc of docs) {
            if(doc.live) {
                liveMode = true;
            }
        }
        Station.find({}).exec()
            .catch(err => res.send(err))
            .then(stations => res.render('admin', { title: 'Adminbereich', stations: stations, liveMode: liveMode }));
    });
});

module.exports = router;
