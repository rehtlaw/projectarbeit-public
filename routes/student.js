const express = require('express'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    middleware = require('../middleware');

// model imports
const Student = require('../models/student');

// init
const router = express.Router();

// main routes
router.route('/')
    // get all users
    .get((req, res) => {
        Student.find({}).exec()
            .catch((err) => res.send(err))
            .then((user) => res.json(user));
    });

router.route('/login')
    .get((req, res) => {
        res.render('login');
    })
    .post(passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
    }), (req, res) => {
        res.json({ message: 'Logged in!' });
    });

router.route('/register')
    .get((req, res) => {
        res.json(middleware.generatePasswords());
    });


module.exports = router;
