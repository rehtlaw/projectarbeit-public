// module imports
const express = require('express'),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

// model imports
const Station = require('../models/station'),
    Student = require('../models/student');

// init
const router = express.Router();

// main routes
router.route('/')
    // GET route
    .get((req, res) => {
        Station.find({}).exec()
            .catch(err => res.send(err))
            .then(station => res.json(station));
    })

    // POST route
    .post((req, res) => {
        let is80mins = false;
        if(req.body.is80minutes) {
            is80mins = true;
        }
        let newStation = { name: req.body.name, room: req.body.room, teacher: req.body.teacher, description: req.body.desc, is80Minutes: is80mins };
        Station.create(newStation)
            .catch(err => res.send(err))
            .then(station => res.redirect('/admin'));
    });

// routes for specific stationen
router.get('/:id', (req, res) => {
    Station.findById(req.params.id).exec()
        .catch(err => res.send(err))
        .then(station => res.json(station));
});

router.post('/:id', (req, res) => {
    Station.findById(req.params.id).exec()
        .catch(err => res.send(err))
        .then(station => {
            station.students.push(req.body.schueler);
            station.save()
                .catch(err => res.send(err))
                .then(() => res.redirect(`/station/${station._id}`));
        });
});

router.post('/edit/:id', (req, res) => {
    Station.findById(req.params.id).exec()
        .catch(err => res.send(err))
        .then(station => {
            station.name = req.body.name;
            station.raum = req.body.raum;
            station.lehrer = req.body.lehrer;
            station.description = req.body.description;
            station.is80Minutes = req.body.is80minutes;
            station.save()
                .catch(err => res.send(err))
                .then(res.redirect(`/station/${station._id}`));
        });
});

router.get('/delete/:id', (req, res) => {
    Station.findByIdAndDelete(new ObjectId(req.params.id), err => {
        if(err) {
            res.redirect('/admin');
        } else {
            res.redirect('/admin');
        }
    });
});

module.exports = router;
