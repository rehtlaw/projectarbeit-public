// module imports
const express = require('express'),
    mongoose = require('mongoose'),
    utf8 = require('utf8'),
    passport = require('passport'),
    multer = require('multer');

// model imports
const Station = require('../models/station'),
    User = require('../models/user'),
    Student = require('../models/student');

// middleware import
const middleware = require('../middleware');
const uploader = multer({ storage: multer.memoryStorage() });

// init router
const router = express.Router();

router.get('/cleardb', (req, res) => {
    // delete all stations
    Station.deleteMany({}).exec()
        .catch(err => res.send(err))
        .then(() => {
            // delete all users
            Student.deleteMany({}).exec()
                .catch(err2 => res.send(err2))
                .then(res.redirect('/admin'));
        });
});

router.post('/upload', uploader.single('csv'), (req, res) => {
    // get file
    const file = req.file.buffer.toString();
    // parse csv
    const data = middleware.readCSV(file);
    // add them to students collection
    Student.create(data, function(err, docs) {
        if(err) {
            res.send(err);
        } else {
            res.redirect('/admin');
        }
    });
});

router.get('/live-mode', (req, res) => {
    Station.create({ live: true }, (err, doc) => {
        if(err) {
            res.send(err);
        } else {
            res.redirect('/');
        }
    });
});

router.get('/reset_pw', (req, res) => {
    User.deleteOne({'username': 'admin'}).exec()
        .catch(err => res.send(err))
        .then(res.redirect('/api/admin/register'));
});

router.get('/register', (req, res) => {
    res.render('register', { title: 'Registrieren'});
});

router.post('/register', (req, res) => {
    let newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, (err, user) => {
        if(err) {
            return res.render('register', { title: 'Registrieren' });
        }
        passport.authenticate('local')(req, res, () => res.redirect('/login'));
    });
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/'
}), (req, res) => {});

module.exports = router;
