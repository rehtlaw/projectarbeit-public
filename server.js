// module imports
const express = require('express'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    methodOverride = require('method-override'),
    LocalStrategy = require('passport-local'),
    bodyParser = require('body-parser');

// model imports
const Station = require('./models/station');
const Student = require('./models/student');
const User = require('./models/user');

// .env import
require('dotenv').config();

// init
const app = express();
const stationRouter = require('./routes/station');
const studentRouter = require('./routes/student');
const viewsRouter = require('./routes/views');
const adminRouter = require('./routes/admin');
const port = process.env.PORT;

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }).catch(err => console.log(err));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

// passport
app.use(require('express-session')({
    secret: 'cHJvamVrdGFyYmVpdA==',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
    res.locals.currentUser = req.user;
    next();
});

// set up routes
app.use('/', viewsRouter);
app.use('/api/station', stationRouter);
app.use('/api/student', studentRouter);
app.use('/api/admin', adminRouter);

// start server
app.listen(port, () => console.log(`server started on ${port}`));
