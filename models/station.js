const mongoose = require('mongoose');

// Station Schema
const stationSchema = new mongoose.Schema({
    name: String,
    room: String,
    teacher: String,
    is80Minutes: Boolean,
    live: Boolean,
    students: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Student'
        }
    ],
    description: String
});

module.exports = mongoose.model('Station', stationSchema);
