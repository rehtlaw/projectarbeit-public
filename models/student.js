const mongoose = require('mongoose'),
    passportLocalMongoose = require('passport-local-mongoose');

// Schueler Schema
let studentSchema = new mongoose.Schema({
    class: String,
    firstName: String,
    lastName: String,
    username: String,
    firstChoice: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Station'
    },
    secondChoice: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Station'
    },
    thirdChoice: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Station'
    },
    fourthChoice: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Station'
    }
});

studentSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('Student', studentSchema);
